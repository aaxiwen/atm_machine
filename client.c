#include <stdio.h>
#include <unistd.h>
#include "msg_queue.h"
#include "status.h"
#include "struct.h"
#include<getch.h>

static int msgid_ctos;
static int msgid_stoc;

static Msg_ctos msg_ctos;
static Msg_stoc msg_stoc;

void msg_send(void)
{
	if(ERROR == msgsnd(msgid_ctos,&msg_ctos,sizeof(msg_ctos)-4,0))
	{
		perror("msgsnd");
		_exit(-1);
	}
}

void msg_recv(void)
{
	if(ERROR == msgrcv(msgid_stoc,&msg_stoc,sizeof(msg_stoc),msg_stoc.m_type,0))
	{
		perror("msgrcv");
		_exit(-1);
	}

	show_msg(msg_stoc.m_str,1.5);
}

void show_menu(void)
{
	system("clear");
	puts("[1]开户	[2]销户");
	puts("[3]登陆	[4]解锁");
	puts("[0]退出");
	puts("-----------------");
}

void show_new(void)
{
	system("clear");
	puts("[5]存钱    [6]取钱");
	puts("[7]查询    [8]转账");
	puts("[9]修改密码       ");
	puts("[0]退出");
	puts("------------------");
}


void open(void)
{
	printf("请输入用户名:");
	get_str(msg_ctos.m_acc.name,sizeof(msg_ctos.m_acc.name));
	
	printf("请输入身份证号：");
	get_pd(msg_ctos.m_acc.shen,sizeof(msg_ctos.m_acc.shen));

	printf("请输入密码:");
	get_pd(msg_ctos.m_acc.pswd,sizeof(msg_ctos.m_acc.pswd));

	printf("请输入预存金额:");
	scanf("%f",&msg_ctos.m_acc.balance);

	getch();
	
	msg_ctos.m_acc.temp=0;
	msg_ctos.m_acc.lock=0;

	msg_ctos.m_type = M_OPEN;
}

void save(void)
{
	printf("请输入帐号:");
	scanf("%d",&msg_ctos.m_acc.id);

	printf("请输入密码:");
	get_pd(msg_ctos.m_acc.pswd,sizeof(msg_ctos.m_acc.pswd));

	printf("请输入存款金额:");
	scanf("%f",&msg_ctos.m_acc.balance);
	
	getch();
	
	msg_ctos.m_type = M_SAVE;
}

void take(void)
{
	printf("请输入帐号:");
	scanf("%d",&msg_ctos.m_acc.id);

	printf("请输入密码:");
	get_pd(msg_ctos.m_acc.pswd,sizeof(msg_ctos.m_acc.pswd));

	printf("请输入取款金额:");
	scanf("%f",&msg_ctos.m_acc.balance);

	getch();

	msg_ctos.m_type = M_TAKE;

}

void query(void)
{
	printf("请输入帐号:");
	scanf("%d",&msg_ctos.m_acc.id);

	printf("请输入密码:");
	get_pd(msg_ctos.m_acc.pswd,sizeof(msg_ctos.m_acc.pswd));

	msg_ctos.m_type = M_QUERY;
}

void transf(void)
{
	printf("请输入帐号:");
	scanf("%d",&msg_ctos.m_acc.id);

	printf("请输入密码:");
	get_pd(msg_ctos.m_acc.pswd,sizeof(msg_ctos.m_acc.pswd));

	printf("请输入要转入的帐号:");
	get_str(msg_ctos.m_acc.name,sizeof(msg_ctos.m_acc.name));

	printf("请输入转账金额:");
	scanf("%f",&msg_ctos.m_acc.balance);

	getch();

	msg_ctos.m_type = M_TRANSF;
}

void destroy(void)
{
	printf("请输入要注销的帐号:");
	scanf("%d",&msg_ctos.m_acc.id);

	printf("请输入要注销的用户名:");
	get_str(msg_ctos.m_acc.name,sizeof(msg_ctos.m_acc.name));

	printf("请输入密码:");
	get_pd(msg_ctos.m_acc.pswd,sizeof(msg_ctos.m_acc.pswd));

	msg_ctos.m_type = M_DESTROY;
}

void deng(void)
{
	printf("请输入帐号:");
	scanf("%d",&msg_ctos.m_acc.id);

	printf("请输入密码:");
	get_pd(msg_ctos.m_acc.pswd,sizeof(msg_ctos.m_acc.pswd));
	
	msg_ctos.m_type = M_DENG;
}

void unlock(void)
{
	 printf("请输入帐号:");
	 scanf("%d",&msg_ctos.m_acc.id);
	
	 printf("请输入身份证号：");
	 scanf("%s",msg_ctos.m_acc.shen);

	 getch();
	
	 msg_ctos.m_type=M_UNLOCK;
}

void change(void)
{
	char pswdnew[10];
	printf("请输入帐号:");
	scanf("%d",&msg_ctos.m_acc.id);

	printf("请输入密码:");
	get_pd(msg_ctos.m_acc.pswd,sizeof(msg_ctos.m_acc.pswd));

	printf("请输入新密码:");
	get_pd(msg_ctos.m_acc.pswdnew,sizeof(msg_ctos.m_acc.pswdnew));
	
	msg_ctos.m_type = M_CHANGE;
}



void shift1()
{
	int flag=0;
	msgid_ctos = get_ctos_msg();
	msgid_stoc = get_stoc_msg();

	msg_stoc.m_type = msg_ctos.m_pid = getpid(); 

	while(true)
	{
		if(flag==0)
		show_menu();
		else
		show_new();
		int i=0;
		switch(get_cmd('0','9') - '0')
		{
			case M_EXIT: return;
			case M_OPEN: open(); break;
			case M_DESTROY:	destroy(); break;
			case M_DENG : deng();break;
			case M_UNLOCK : unlock();break;
			case M_SAVE:save();break;
			case M_TAKE:take();break;
			case M_QUERY : query();break;
			case M_TRANSF:transf();break;
			case M_CHANGE:change();break;
		}

		msg_send();
		msg_recv();
		getch();
		if(strcmp(msg_stoc.m_str,"登陆成功..."));
			flag=1;
	}
}





int main()
{
	shift1();
}
