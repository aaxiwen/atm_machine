all:
	gcc client.c msg_queue.c tools.c -o client
	gcc server.c msg_queue.c tools.c -o server
	gcc open.c msg_queue.c tools.c -o open
	gcc query.c msg_queue.c -o query
	gcc destroy.c msg_queue.c -o destroy
	gcc unlock.c msg_queue.c -o unlock
	gcc deng.c msg_queue.c -o deng
	gcc save.c msg_queue.c -o save
	gcc take.c msg_queue.c -o take
	gcc transf.c msg_queue.c -o transf
	gcc change.c msg_queue.c -o change
clean:
	rm client server open save take query transf destroy
	
