#include <stdio.h>
#include <unistd.h>
#include <signal.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include "msg_queue.h"
#include "status.h"
#include "struct.h"
#include "tools.h"

static int msgid_ctos;
static int msgid_stoc;
static pid_t sub_pid[10];

void sigint_ctos_func(int signum)
{
	msgctl(msgid_ctos,IPC_RMID,NULL);
	msgctl(msgid_stoc,IPC_RMID,NULL);
	
	int i;
	for(i=0; i<10; i++)
	{
		kill(sub_pid[i],SIGINT);
	}
}

int main()
{
	signal(SIGINT,sigint_ctos_func);

	init_id(ID_PATH,201700000);

	msgid_ctos = get_ctos_msg();
	msgid_stoc = get_stoc_msg();
	
	sub_pid[0] = vfork();
	if(0 == sub_pid[0])
	{
		execl("open","open",NULL);
	}

	sub_pid[1] = vfork();
	if(0 == sub_pid[1])
	{
		execl("save","save",NULL);
	}

	sub_pid[2] = vfork();
	if(0 == sub_pid[2])
	{
		execl("take","take",NULL);
	}
	
	sub_pid[3] = vfork();
	if(0 == sub_pid[3])
	{
		execl("query","query",NULL);
	}
	sub_pid[4] = vfork();
	if(0 == sub_pid[4])
	{
		execl("transf","transf",NULL);
	}
	sub_pid[5] = vfork();
	if(0 == sub_pid[5])
	{
		execl("destroy","destroy",NULL);
	}
	sub_pid[6]=vfork();
	if(0 == sub_pid[6])
	{
		execl("deng","deng",NULL);
	}
	sub_pid[7]=vfork();
	if(0 == sub_pid[7])
	{
		execl("unlock","unlock",NULL);
	}
	sub_pid[8]=vfork();
	if(0 == sub_pid[8])
	{
		execl("transf","transf",NULL);
	}
	sub_pid[9]=vfork();
	if(0 == sub_pid[9])
	{
		execl("change","change",NULL);
	}
	wait();
}











