#ifndef STRUCT_H
#define STRUCT_H

#define M_EXIT		0	// 退出
#define	M_OPEN		1 	// 开户
#define M_DESTROY	2 	// 销户
#define M_DENG      3   //登陆
#define M_UNLOCK    4  //解锁
#define M_SAVE		5 	// 存钱
#define M_TAKE		6	// 取钱
#define M_QUERY		7	// 查询
#define M_TRANSF	8	// 转账
#define M_CHANGE	9	//修改密码
#define M_SUCCESS	10 	// 成功
#define M_FAILED	11 	// 失败



typedef struct//帐户信息结构体
{
	int 	id;			// 帐号 
	char 	shen[20];
	char 	name[10];	// 用户名
	char 	pswd[10];	// 密码
	char  	pswdnew[10] ;
	float	balance;	// 金额
	int 	temp;
	int 	lock;
}Acc;

typedef struct//客户端到服务器端的结构体
{
	int 	m_type;		//	消息类型
	Acc 	m_acc;		//	帐户信息
	pid_t 	m_pid;		//	客户端进程号
}Msg_ctos;

typedef struct//服务器端到客户端的结构体
{
	int 	m_type;		// 消息类型
	char 	m_str[255];	// 提示信息
}Msg_stoc;

#endif//STRUCT_H
